package com.example.ezrra.fragmento;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
// import android.support.v7.app.ActionBarActivity; // Revisar por que me mando de aun inicio esa clase y por que no se puede imoprtar
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {

    // Codigo del Fragmento de Ejecucion CFE

    //CFE
    private Button add_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // CFE
        add_fragment = (Button)findViewById(R.id.add_fragment);

        add_fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 1
                FragmentManager fragmentManager = getFragmentManager();
                // 2
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                // 3
                CountFragment fragment = new CountFragment();
                transaction.add(R.id.container, fragment);
                // 4
                transaction.commit();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
