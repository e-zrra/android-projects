package com.example.ezrra.iwish_ezrra;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.example.ezrra.iwish_ezrra.MainFragment;
import com.example.ezrra.iwish_ezrra.Constantes;
import com.example.ezrra.iwish_ezrra.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creación del fragmento principal

        if (savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new MainFragment(), "MainFragment").commit();
            // getSupportFragmentManager().beginTransaction().add(R.id.container, new MainFragment(),"MainFragment").commit();
        }
    }


}
