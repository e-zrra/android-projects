package com.example.ezrra.iwish_ezrra;


import android.os.Bundle;
import android.support.v4.app.Fragment; // Investigar por que ocupa la version 4 ???
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
// import android.app.Fragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by luis on 4/8/16.
 */
public class MainFragment extends Fragment {


    // Etiqueta de depuración.
    private static final String TAG = MainFragment.class.getSimpleName();

    // Adaptador del recycler view
    private MetaAdapter adapter;

    // Instancia global del recycler view
    private RecyclerView lista;

    // instancia global del administrador
    private RecyclerView.LayoutManager lManager;

    // Isntancia global del FAB
    com.melnykov.fab.FloatingActionButton fab;

    private Gson gson = new Gson();

    public MainFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_main, container, false);

        lista = (RecyclerView) v.findViewById(R.id.reciclador);
        lista.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        lista.setLayoutManager(lManager);

        // Cargar datos en el adaptador
        cargarAdaptador();

        // Obtener instancia del FAB
        fab = (com.melnykov.fab.FloatingActionButton) v.findViewById(R.id.fab);

        // Asignar escucha al FAB
        fab.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Iniciar actividad de inserción
                        // getActivity().startActivityForResult(new Intent(getActivity(), InsertActivity.class), 3);
                    }
                }
        );

        return v;
    }

    /**
     * Carga el adaptador con las metas obtenidas
     * en la respuesta
     */

    public void cargarAdaptador () {

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(
                new JsonObjectRequest(Request.Method.GET,
                        Constantes.GET, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        procesarRespuesta(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Volley: " + error.getMessage());
                    }
                })
        );
    }

    /**
     * Interpreta los resultados de la respuesta y así
     * realizar las operaciones correspondientes
     *
     * @param response Objeto Json con la respuesta
     */

    private void procesarRespuesta (JSONObject response) {

        try {

            // Obtener atributo "estado"

            String estado = response.getString("estado");

            switch (estado) {

                case "1":
                    // Obtener el array metas Json
                    JSONArray mensaje = response.getJSONArray("metas");

                    // Parsear con Gson
                    Meta[] metas = gson.fromJson(mensaje.toString(), Meta[].class);

                    //Iniciar el adaptador a la lista
                    adapter = new MetaAdapter(Arrays.asList(metas), getActivity());

                    // setear adaptador a la lista
                    lista.setAdapter(adapter);

                    break;
                case "2":
                    String mensaje_error = response.getString("mensaje");

                    Toast.makeText(getActivity(), mensaje_error, Toast.LENGTH_LONG).show();
                    break;
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }
    }


}
