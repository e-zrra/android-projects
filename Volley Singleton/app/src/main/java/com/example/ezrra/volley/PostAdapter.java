package com.example.ezrra.volley;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

public class PostAdapter extends ArrayAdapter {

    // Atributos
    private static final String URL_BASE = "http://servidorexterno.site90.com/datos";
    private static final String URL_JSON = "/social_media.json";
    private static final String TAG = "PostAdapter";
    ListaPost items;

    public PostAdapter(Context context) {
        super(context, 0);

        // Añadir peticiones GSON a la cola
        MySocialMediaSingleton.getInstance(getContext()).addToRequestQueue(
                new GsonRequest<ListaPost>(URL_BASE + URL_JSON, ListaPost.class, null, new Response.Listener<ListaPost>() {
                    @Override
                    public void onResponse(ListaPost response) {
                        items = response;
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Volley:"+ error.getMessage());
                    }
                })
        );
    }

    @Override
    public int getCount()
    {
        return items != null ? items.getItems().size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // View Auxiliar
        View listItemView;

        //Comprobar si el view no existe
        if (null == convertView)
        {
            listItemView =  layoutInflater.inflate(R.layout.post, parent, false);

        } else {

            listItemView =  convertView;
        }

        // Obtener el item actual
        Post item = items.getItems().get(position);
        // Verificar si es necesario validar si existe o no un item.

        // Obtener Views
        TextView textoTitulo = (TextView)listItemView.findViewById(R.id.textoTitulo);
        TextView textoDescripcion = (TextView)listItemView.findViewById(R.id.textoDescripcion);
        NetworkImageView imagenPost = (NetworkImageView) listItemView. findViewById(R.id.imagenPost);

        // Actualizar los Views
        textoTitulo.setText(item.getTitulo());
        textoDescripcion.setText(item.getDescripcion());

        // Peticion a la imagen loader
        ImageLoader imageLoader = MySocialMediaSingleton.getInstance(getContext()).getImageLoader();

        // Peticion
        imagenPost.setImageUrl(URL_BASE + item.getImagen(), imageLoader);

        return listItemView;
    }

}
