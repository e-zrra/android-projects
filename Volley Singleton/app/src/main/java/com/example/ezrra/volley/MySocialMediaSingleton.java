package com.example.ezrra.volley;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by ezrra on 26/03/16.
 */
public final class MySocialMediaSingleton {

    // Atributos
    private static MySocialMediaSingleton singleton;
    private RequestQueue requestQueue;
    private static Context context;
    private ImageLoader imageLoader;

    private MySocialMediaSingleton (Context context) {

        MySocialMediaSingleton.context = context;
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue,
                        new ImageLoader.ImageCache(){
                            private final LruCache<String, Bitmap> cache = new LruCache<>(20);

                            @Override
                            public  Bitmap getBitmap(String url) {
                                return cache.get(url);
                            }

                            @Override
                            public void putBitmap(String url, Bitmap bitmap) {
                                cache.put(url, bitmap);
                            }
                        });
    }

    /**
     * Simplemente asigna memoria a la unica instancia del singleton, donde se llama al constructor privado
     * de la clase.
     * Este metodo tiene la propiedad synchronized: ya que la instancia sera accedida desde varios hilos
     * por lo que es necesario evitar bloqueos de acceso.
     * @param context
     * @return
     */
    public static synchronized MySocialMediaSingleton getInstance( Context context) {

        if (singleton == null) {
            singleton = new MySocialMediaSingleton(context);
        }
        return singleton;
    }

    /**
     * Obtiene la instancia de la cola de peticiones que usara a traves de toda la aplicacion.
     * @return
     */
    public RequestQueue getRequestQueue () {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    /**
     * Para agregar nueva peticion, una ves que la instancia este creada.
     * @param req
     */
    public void addToRequestQueue(Request req) {

        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader () {

        return imageLoader;
    }


}
