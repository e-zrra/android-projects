package com.example.ezrra.volley;

import java.util.List;
/**
 * Created by ezrra on 27/03/16.
 * Encapsule la lista de elementos
 */

public class ListaPost {

    // Encapsulamiento de Post
    private List<Post> items;

    public ListaPost(List<Post> items) {

        this.items = items;
    }

    public void setItems(List<Post> items) {

        this.items = items;
    }

    public List<Post> getItems () {

        return items;
    }

}
