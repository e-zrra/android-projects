package com.example.ezrra.cruder;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.app.ActionBar;

import org.w3c.dom.Text;

public class MainActivity extends ActionBarActivity {

    private TextView info;

    //Obteniendo la instancia
    // ActionBar actionBar = getActionBar();
    //Escondiendo la Action Bar
    // actionBar.hide();
    //Mostrando de nuevo la Action Bar
    // actionBar.show();
    //Seteando el icono
    // actionBar.setIcon(R.drawable.logo);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // obteniendo la instancia del textview
        info = (TextView)findViewById(R.id.info);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Menu es equivalente <menu>
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true; // All salio como se esperaba
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                //metodo add
                info.setText("Se presiono añadir");
                return true;
            case R.id.search:
                //metodoSearch()
                info.setText("Se presionó Buscar");
                return true;
            case R.id.edit:
                //metodoEdit()
                info.setText("Se presionó Editar");
                return true;
            case R.id.delete:
                //metodoDelete()
                info.setText("Se presionó Eliminar");
                return true;
            case R.id.action_settings:
                //metodoSettings()
                info.setText("Se presionó Ajustes");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        // MenuItem es equivalente <item>
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Comentado lo siguiente
        // int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        // Comentado lo siguiente
        /*
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item); */
    }
}
