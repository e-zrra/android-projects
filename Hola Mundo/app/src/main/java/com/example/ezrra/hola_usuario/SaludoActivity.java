package com.example.ezrra.hola_usuario;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import android.widget.TextView;

public class SaludoActivity extends ActionBarActivity {

    private TextView txtSaludo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saludo);

        //Localizar los controles
        txtSaludo = (TextView)findViewById(R.id.TxtSaludo);

        // Recupera la informacion pasada en el intent
        Bundle bundle = this.getIntent().getExtras();
        txtSaludo.setText("Hola " + bundle.getString("NOMBRE"));

    }

}
