package com.example.ezrra.hola_usuario;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button; // Agregado
import android.widget.EditText; // Agregado
import android.content.Intent; // Agregado

public class MainActivity extends ActionBarActivity {
    // Codigo
    private EditText txtNombre;
    private Button btnAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Codigo
        // Obtendremos una referencia a los controles de la interfaz
        txtNombre = (EditText)findViewById(R.id.TxtNombre);
        btnAceptar = (Button)findViewById(R.id.BtnAceptar);

        // Codigo
        // Implementar el evento on click del boton

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Creamos el Intent - Codigo
                Intent intent = new Intent(MainActivity.this, SaludoActivity.class);

                // Creamos la informacion a pasar entre acividades
                Bundle b = new Bundle();
                b.putString("NOMBRE", txtNombre.getText().toString());

                // Añadir la informacion al intent - Codigo
                intent.putExtras(b);

                // Iniciamos la nueva actividad
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
