package com.example.ezrra.petshowintents;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.net.Uri;
import java.util.List;

public class MainActivity extends Activity implements OnClickListener {

    public final static int OPINION_REQUEST_CODE = 1;
    // public final static String EXTRA_NOMBRE = "com.example.ezrra.petshowintents.NAME";
    public final static String EXTRA_NOMBRE = "pet1.jpg";


    private Button show_pet_button;
    private TextView opinion_text;
    private TextView page_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        show_pet_button = (Button)findViewById(R.id.show_pet_button);
        page_link = (TextView)findViewById(R.id.page_link);
        opinion_text = (TextView)findViewById(R.id.opinion_text);

        // Registrando la escucha sobre la actividad Main

        show_pet_button.setOnClickListener(this);
        page_link.setOnClickListener(this);

        // Ejecutando el intent
        /*
            Package
            Uri webpage = Uri.parse("http://hermosaprogramacion.blogspot.com");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);

            PackageManager packageManager = getPackageManager();
            List activities = packageManager.queryIntentActivities(webIntent, 0);
            boolean isIntentSafe = activities.size() > 0;

            // Si existe se ejecutam la actividad
            if (isIntentSafe) {
                startActivity(webIntent);
            }
        */
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.show_pet_button: {
                // Iniciar la actividad Visor
                Intent intent = new Intent(this, Visor.class);
                // Adhesion de nuestra cadena
                intent.putExtra(EXTRA_NOMBRE, "pet1.jpg");
                // Iniciando de la actividad esperando un resultado
                startActivityForResult(intent, OPINION_REQUEST_CODE);

                break;
            }
            case R.id.page_link: {

                Uri webpage = Uri.parse("http://hermosaprogramacion.blogspot.com");

                Intent webIntent = new Intent (Intent.ACTION_VIEW, webpage);
                // Ver si hay intents disponibles
                PackageManager packageManager = getPackageManager();
                List activities = packageManager.queryIntentActivities(webIntent, 0);
                boolean isIntentSafe = activities.size() > 0;
                // Si hay se ejecuta la actividad
                if (isIntentSafe) {
                    startActivity(webIntent);
                }
                break;
            }
        }
        //Iniciando la actividad Visor
        // Intent intent = new Intent(this,Visor.class);
        //adherimos la cadena
        // intent.putExtra(EXTRA_NOMBRE, "pet2.jpg");
        // startActivity(intent);

    }

    /**
     * Sobreescirbimos  este metodo que se autoinvoca para recibir la informacion de una actividad que fue llamada
     * con un StartActivityForResult()
     * En este metodo comprobamos que all este bien, y luego obtendremos los datos del intent de respuesta
     **/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OPINION_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {

                String result = data.getStringExtra("opinion");

                opinion_text.setText("Tu opinon es: " + result);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
