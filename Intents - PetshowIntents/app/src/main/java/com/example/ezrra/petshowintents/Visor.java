package com.example.ezrra.petshowintents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.w3c.dom.Text;

public class Visor extends Activity implements View.OnClickListener {

    private TextView image_name;
    private RadioGroup opiniones;
    private Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor);

        // Instancia del nuestros views
        image_name = (TextView)findViewById(R.id.image_name);
        opiniones = (RadioGroup)findViewById(R.id.opiniones_group);
        enviar = (Button)findViewById(R.id.send_button);


        //Obtener la instancia del Intent
        Intent intent = getIntent();
        //Extrayendo el extra de tipo cadena
        String name = intent.getStringExtra(MainActivity.EXTRA_NOMBRE);
        //Seteando el valor del extra en el Texview
        image_name.setText(name);
        //Añadiendo escucha al boton send_button
        enviar.setOnClickListener(this);

        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View v) {

        // Obtener del radiobutton seleccionado actualmente
        RadioButton currentRadio = (RadioButton)findViewById(opiniones.getCheckedRadioButtonId());

        // Obtener la cadena del radibutton
        String opinion =  currentRadio.getText().toString();

        // Crear un nuevo intent de respuesta
        Intent databack = new Intent();

        //Añadir como Extra el texto del radiobutton
        databack.putExtra("opinion", opinion);

        //Devolver por el canal de forma exitosa el mensaje del intent
        setResult(RESULT_OK, databack);

        //Terminar la actividad
        finish();


    }

}
