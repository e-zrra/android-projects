package com.example.ezrra.quotilius_ezrra;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SimpleCursorAdapter;
import android.app.ListActivity;

import com.example.ezrra.quotilius_ezrra.QuotesDataSource.ColumnQuotes;

public class MainActivity extends ListActivity {

    /* No Finalizado :( */

    // Codigo de envio
    public final static int ADD_REQUEST_CODE = 1;
    // Atributos para datos DB;
    private QuotesDataSource dataSource;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Crear nuevo objeto QuotesDataSource // Se crea la base de datos 
        dataSource = new QuotesDataSource(this);

        // Iniciar el nuevo adaptador
        /* adapter = new SimpleCursorAdapter(this,
                android.R.layout.two_line_list_item,
                dataSource.getAllQuotes(),
                new String[]{ColumnQuotes.BODY_QUOTES,ColumnQuotes.AUTHOR_QUOTES},
                new int[]{android.R.id.text1, android.R.id.text2},
                SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        setListAdapter(adapter); */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_add:
                initForm();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // CODIGO VIEJO
        // int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        // CODIGO VIEJO
        /* if (id == R.id.action_settings) {
            return true;
        } */

        // CODIGO VIEJO
        // return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {

                //Insertando el registro con los datos del formulario
                String body = data.getStringExtra("body");
                String author = data.getStringExtra("author");

                // dataSource.saveQuoteRow(body,author);
                //Refrescando la lista manualmente
                adapter.changeCursor(dataSource.getAllQuotes());
            }
        }
    }

    private void initForm()
    {
        // Iniciamos la actividad Form
        Intent intent = new Intent(this, Form.class);

        //Inicio de la actividad esperando un resultado
        startActivityForResult(intent, ADD_REQUEST_CODE);
    }

}
