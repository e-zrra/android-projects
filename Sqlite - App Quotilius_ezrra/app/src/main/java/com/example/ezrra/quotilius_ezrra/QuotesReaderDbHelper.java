package com.example.ezrra.quotilius_ezrra;

import android.content.Context; // Contexto Revisar
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ezrra on 20/03/16.
 */
public class QuotesReaderDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Quotes.db";
    public static final int DATABASE_VERSION = 1;

    public QuotesReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate (SQLiteDatabase db) {
        // Crear la base de datos
        db.execSQL(QuotesDataSource.CREATE_QUOTES_SCRIPT);
        // Insertar registros iniciales
        db.execSQL(QuotesDataSource.INSERT_QUOTES_SCRIPT);
    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
        // Actualizar la base de datos

    }

}
