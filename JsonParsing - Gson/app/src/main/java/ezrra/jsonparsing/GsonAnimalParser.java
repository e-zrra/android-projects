package ezrra.jsonparsing;

// import android.util.JsonReader;
import com.google.gson.stream.JsonReader;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luis on 3/24/16.
 */
public class GsonAnimalParser {

    public List leerFlujoJson (InputStream in) throws IOException {

        // Nueva instancia de la clase Gson
        Gson gson = new Gson();

        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        List<Animal> animales = new ArrayList<>();

        // Iniciar array
        reader.beginArray();

        while (reader.hasNext()) {

            // Lectura de objetos
            // 1. Lector y la Clase lo convierte o relaciona.
            // 2. Lo convertirmos a la clase
            // 3. Lo agregamos a nuestro Listado de la misma clase.

            Animal animal = gson.fromJson(reader, Animal.class);

            animales.add(animal);
        }

        reader.endArray();

        reader.close();

        return animales;

    }
}
