package ezrra.jsonparsing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by luis on 3/24/16.
 */
public class AdaptadorDeAnimales extends ArrayAdapter<Animal> {

    public AdaptadorDeAnimales(Context context, List<Animal> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View converView, ViewGroup parent) {

        // Obteniendo la instancia del inflador
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Salvando la referencia del View de la fila
        View v = converView;

        // Comprobando si el view no existe.

        if (null == converView) {

            // Si no existe, entonces inflarlo.
            v = inflater.inflate(R.layout.item_lista, parent, false);
        }

        // Obtener la instancia de los elementos
        TextView especieAnimal = (TextView)v.findViewById(R.id.especieAnimal);
        TextView descAnimal = (TextView)v.findViewById(R.id.descAnimal);
        ImageView imagenAnimal = (ImageView)v.findViewById(R.id.imagenAnimal);

        // Obteniendo instancia de la Tarea en la posicion actual
        Animal item = getItem(position);

        especieAnimal.setText(item.getEspecie());
        descAnimal.setText(item.getDescripcion());
        imagenAnimal.setImageResource(convertirRutaEnId(item.getImagen()));

        // Devolver la listView la fila creada
        return v;
    }

    private int convertirRutaEnId (String nombre) {

        Context context = getContext();

        return context.getResources().getIdentifier(nombre, "drawable", context.getPackageName());
    }

}
