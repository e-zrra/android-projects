package ezrra.jsonparsing;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity {

    /*
    Variables globales
     */
    ListView lista;
    ArrayAdapter adaptador;
    HttpURLConnection con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista= (ListView) findViewById(R.id.listaAnimales);

        /* Comprobar la disponibilidad de la red */
        try {

            ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {

                new JsonTask().execute(new URL("http://servidorexterno.site90.com/datos/get_all_animals.php"));

            } else {

                Toast.makeText(this, "Error de conexión", Toast.LENGTH_LONG).show();
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class JsonTask extends AsyncTask<URL, Void, List<Animal>> {

        @Override
        protected List<Animal> doInBackground(URL... urls) {

            List<Animal> animales = null;

            try {

                // Establecer la conexion

                con = (HttpURLConnection)urls[0].openConnection();

                con.setConnectTimeout(15000);

                con.setReadTimeout(10000);

                // Obtener el estado del recurso
                int statusCode = con.getResponseCode();

                if(statusCode!=200) {

                    animales = new ArrayList<>();

                    animales.add(new Animal("Error", null, null));

                } else {

                    // Parsear el flujo con formato JSON
                    InputStream in = new BufferedInputStream(con.getInputStream());

                    // Parseo JsonReader - Error Revisar
                    // JsonAnimalParser parser = new JsonAnimalParser();
                    // animales = parser.readJsonStream(in); // parser.leerFlujoJson(in);

                    // Parseo GSon
                    GsonAnimalParser parser = new GsonAnimalParser();
                    animales = parser.leerFlujoJson(in);

                }

            } catch (Exception e) {

                e.printStackTrace();

            } finally {

                con.disconnect();
            }

            return animales;
        }

        @Override
        protected void onPostExecute (List<Animal> animales) {

            /* Asignar objetos de Json parseados al adaptador*/

            if (animales != null) {

                adaptador = new AdaptadorDeAnimales(getBaseContext(), animales);

                lista.setAdapter(adaptador);

            } else {

                Toast.makeText(getBaseContext(), "Ocurrio un error de Parseo Json", Toast.LENGTH_LONG).show();

            }
        }
    }
}

/*

JSON

[{"especie":"Le\u00f3n","descripcion":"El le\u00f3n (Panthera leo) es un mam\u00edfero carn\u00edvoro de la familia de los f\u00e9lidos y una de las 5 especies del g\u00e9nero Panthera. Algunos machos, excepcionalmente grandes, llegan a pesar hasta 250 kg, lo que los convierte en el segundo f\u00e9lido viviente m\u00e1s grande ","imagen":"leon"},{"especie":"Elefante","descripcion":"Los elefantes o elef\u00e1ntidos (Elephantidae) son una familia de mam\u00edferos placentarios del orden Proboscidea. Antiguamente se clasificaban, junto con otros animales de piel gruesa, en el orden, ahora inv\u00e1lido, de los paquidermos (Pachydermata). ","imagen":"elefante"},{"especie":"Cocodrilo","descripcion":"Los crocod\u00edlidos (Crocodylidae) son una familia de saur\u00f3psidos (reptiles) arcosaurios com\u00fanmente conocidos como cocodrilos. Incluye a 14 especies actuales.Se trata de grandes reptiles semiacu\u00e1ticos que viven en las regiones tropicales de \u00c1frica y Asia","imagen":"cocodrilo"},{"especie":"Cebra","descripcion":"Se conocen como cebra (o zebra, graf\u00eda en desuso ) a tres especies del g\u00e9nero Equus propias de \u00c1frica, Equus quagga (cebra com\u00fan), Equus grevyi (cebra de Grevy) y Equus zebra (cebra de monta\u00f1a); al mismo g\u00e9nero que pertenecen tambi\u00e9n los caballos","imagen":"cebra"},{"especie":"\u00c1guila","descripcion":"El \u00e1guila calva (Haliaeetus leucocephalus), tambi\u00e9n conocida como \u00e1guila americana, \u00e1guila de cabeza blanca, pigargo de cabeza blanca o pigargo americano, es una especie de ave accipitriforme de la familia Accipitridae que habita en Am\u00e9rica del Norte","imagen":"aguila"}]

*/
