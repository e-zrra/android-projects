package ezrra.jsonparsing;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luis on 3/24/16.
 */
public class JsonAnimalParser {


    /**
     * El retorno es un lista de Animales
     * @param in
     * @return
     * @throws IOException
     */
    public List<Animal> readJsonStream(InputStream in) throws IOException {

        // Nueva instancia de JsonReader
        JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));

        try {

            // Leer Array
            return leeArrayAnimales(reader);

        } finally {

            // Limpiar espacios de memoria referenciada
            reader.close();

        }
    }

    /**
     * Leer el array de objetos de Json
     * Recorrer all el array enviado desde el servidor.
     * Resive como parametro lector Json para continuar la labor de lectura
     * @param reader
     * @return
     * @throws IOException
     */
    public List leeArrayAnimales (JsonReader reader) throws IOException {

        // Lista Temporal
        ArrayList animales = new ArrayList();

        reader.beginArray();

        while (reader.hasNext()) {

            // Leer objeto
            animales.add(leerAnimal(reader));

        }

        reader.endArray();

        return animales;
    }

    /**
     * Leer los atributos de cada objeto Json
     *
     */

    public Animal leerAnimal (JsonReader reader) throws IOException {

        String especie = null;
        String descripcion = null;
        String imagen = null;

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            switch (name) {

                case "especie":
                    especie = reader.nextString();
                    break;
                case "descripcion":
                    descripcion = reader.nextString();
                    break;
                case "imagen":
                    imagen = reader.nextString();
                    break;
                default:
                    reader.skipValue();
                    break;
            }
        }

        reader.endObject();

        return new Animal(especie, descripcion, imagen);
    }
}
