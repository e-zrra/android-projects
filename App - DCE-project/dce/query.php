<?php 

header("Content-Type: application/json");

require_once('config.php');


$connection = mssql_connect(HOSTNAME, USERNAME, PASSWORD);


if($connection != FALSE) {

	// echo "Connected to the database server.";

} else {

	die("Couldn't connect");
}

if(mssql_select_db(DATABASE, $connection)) {

	// echo "Selected $database,";

} else {

	die('Failed to select Database.');
}

$data = array();

if (!isset($_GET['number'])) {

    $data['materials'][0] = array('number' => 0,
                                'location' => 'Not found',
                                'unrestricted_quantity' => 0,
                                'block_quantity');
    echo json_encode($data);
    return false;
}

$number = $_GET['number'];
//37680A
$query = "SELECT MATERIAL, STORAGE_LOCATION, UNRESTRICTED, BLOCKED
		  FROM STOCK 
		  WHERE (UNRESTRICTED + BLOCKED > 0) AND  MATERIAL LIKE '%$number%'
		  GROUP BY MATERIAL, STORAGE_LOCATION, UNRESTRICTED, BLOCKED";

// $query = "SELECT MATERIAL, STORAGE_LOCATION, BLOCKED
//           FROM STOCK 
//           WHERE (UNRESTRICTED + BLOCKED > 0) AND  MATERIAL LIKE '%$number%'
//           GROUP BY MATERIAL, STORAGE_LOCATION, BLOCKED";

// (UNRESTRICTED + BLOCKED > 0) AND 

$result = mssql_query($query, $connection);

// while ($fila = mssql_fetch_row($result)) {
//     echo "{$fila[0]}\n";
//     echo "{$fila[1]}\n";
//     echo "{$fila[2]}\n";
//     echo "{$fila[3]}\n";
// }

// echo mssql_num_rows($result);

// return false;

$count 	= mssql_num_rows($result);

// echo "Showing $count rows:<hr/>\n\n";

// return false;

if (!mssql_num_rows($result)) {

    // echo 'No records found.';

    $data['materials'][0] = array('number' => 0,
                                'location' => 'Not found',
                                'unrestricted_quantity' => 0,
                                'block_quantity' => 0);

} else {

    $rows = array();
    
    while ($row = mssql_fetch_array($result, MSSQL_NUM)) {

        $row = array(  'number'                 => $row[0],
            'location'              => (isset($row[1])) ? $row[1] : 'Not found',
            'unrestricted_quantity' => (isset($row[2])) ? $row[2] : 0,
            'block_quantity'        => (isset($row[3])) ? $row[3] : 0
        );
    	array_push($rows, $row); 
    }

    $data['materials'] = $rows;
}

echo json_encode($data);

?>