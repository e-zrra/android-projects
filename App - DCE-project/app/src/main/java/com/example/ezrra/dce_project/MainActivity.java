package com.example.ezrra.dce_project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private Button  btnSearchMaterial;
    private EditText editTextNumber;
    public final static String EXTRA_NUMBER = "";
    private Button btnScanner;
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSearchMaterial = (Button)findViewById(R.id.btnSearchButton);
        btnSearchMaterial.setOnClickListener(this);

        btnScanner = (Button)findViewById(R.id.btnScanner);
        btnScanner.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSearchButton: {

                editTextNumber = (EditText)findViewById(R.id.editTextNumber);

                String number = editTextNumber.getText().toString().trim();

                if (number.equals("") || number == null) {

                    Toast.makeText(this, "Ingresa el número de material", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent intent = new Intent(this, VisorResult.class);
                    intent.putExtra(EXTRA_NUMBER, number);
                    startActivityForResult(intent, 0);
                }
                break;
            }
            case R.id.btnScanner: {
                Intent intent = new Intent(this, ScanActivity.class);
                startActivity(intent); // startActivityForResult(intent, 0);
                break;
            }
        }
    }

}
