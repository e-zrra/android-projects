package com.example.ezrra.dce_project;

/**
 * Created by ezrra on 1/04/16.
 */
public class Material {

    // Atributos
    private String number;
    private String location;
    private String unrestricted_quantity;
    private String block_quantity;

    public Material () {}

    public Material (String number, String location, String unrestricted_quantity, String block_quantity)
    {
        this.number     = number;
        this.location   = location;
        this.unrestricted_quantity     = unrestricted_quantity;
        this.block_quantity = block_quantity;
    }

    public String getNumber () { return number; }

    public void setNumber (String number) { this.number = number; }

    public String getLocation () { return location; }

    public void setLocation (String location) { this.location = location; }

    public String getUnrestrictedQuantity() { return unrestricted_quantity; }

    public void setUnrestrictedQuantity(String amount) { this.unrestricted_quantity = unrestricted_quantity; }

    public String getBlockQuantity() { return block_quantity; }

    public void setBlockQuantity(String amount) { this.block_quantity = block_quantity; }
}
