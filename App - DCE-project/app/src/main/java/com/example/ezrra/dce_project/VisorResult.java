package com.example.ezrra.dce_project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class VisorResult extends AppCompatActivity {

    ListView listView;
    ArrayAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visor_result);

        Intent intent = getIntent();

        String number = intent.getStringExtra(MainActivity.EXTRA_NUMBER);

        // Toast.makeText(this, "Buscando " + number + " ... ", Toast.LENGTH_LONG).show();

        // Obtener la instancia del ListView
        listView = (ListView)findViewById(R.id.listViewMaterials);

        // Crear y setear el adaptador
        adapter = new MaterialAdapter(this, number);

        listView.setAdapter(adapter);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
