package com.example.ezrra.dce_project;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luis on 4/1/16.
 */
public class MaterialAdapter extends ArrayAdapter {

    /*
    * http://201.140.131.78/dce
    * http://192.168.4.110/dce/
    * http://192.168.1.30/dce
    * */

    // Atributos
    private RequestQueue requestQueue;
    JsonObjectRequest jsArrayRequest;
    private static final String URL_BASE = "http://201.140.131.78/dce/";
    private static final String URL_MATERIALS = "/query.php";
    private static final String TAG = "MaterialAdapter";
    Context ct;
    VisorResult activity;
    private ProgressDialog pDialog;
    List<Material> items;

    public MaterialAdapter (Context context, String number_extra) {
        super(context, 0);

        ct = getContext();

        pDialog = new ProgressDialog(ct);
        pDialog.setMessage("Buscando ...");
        pDialog.setCancelable(false);

        // Nueva cola de petición
        requestQueue = Volley.newRequestQueue(context);

        String url =  URL_BASE + URL_MATERIALS + "?number=" + number_extra;

        showpDialog();

        // Nuena petición JSON y con number_extra
        jsArrayRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        items = parseJson(response);
                        hidepDialog();
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError e) {
                        Log.d(TAG, "Error respuesta en JSON: " + e.getMessage());
                        Toast.makeText(ct, "No se encontro resultado. Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        hidepDialog();
                    }
                });

        requestQueue.add(jsArrayRequest);

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public int getCount()
    {
        return items != null ? items.size() : 0;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // Salvando la referencia del view de la fila
        View listItemView = convertView;

        // Comprobar si el view no existe
        listItemView = null == convertView ? layoutInflater.inflate(R.layout.list_item, parent, false) : convertView;

        Material item = items.get(position);

        TextView number = (TextView)listItemView.findViewById(R.id.number);
        TextView location = (TextView)listItemView.findViewById(R.id.location);
        TextView unrestrictedQuantity = (TextView)listItemView.findViewById(R.id.unrestrictedQuantity);
        TextView blockQuantity = (TextView)listItemView.findViewById(R.id.blockQuantity);

        number.setText(item.getNumber());
        location.setText(item.getLocation());
        unrestrictedQuantity.setText(format_decimal(item.getUnrestrictedQuantity()));
        blockQuantity.setText(format_decimal(item.getBlockQuantity()));

        return listItemView;
    }

    public String format_decimal (String quantity) {

        Float litersOfPetrol=Float.parseFloat(quantity);
        DecimalFormat df = new DecimalFormat("0.0");
        df.setMaximumFractionDigits(0);
        quantity = df.format(litersOfPetrol);

        return quantity;
    }

    public List<Material> parseJson (JSONObject jsonObjetc) {

        // Variables locales
        List<Material> materials = new ArrayList<>();
        JSONArray jsonArray = null;

        try {
            // Obtener el array del objeto
            jsonArray = jsonObjetc.getJSONArray("materials");

            for (int i = 0; i < jsonArray.length(); i++) {

                try {

                    JSONObject objeto = jsonArray.getJSONObject(i);

                    Material material = new Material(
                            objeto.getString("number"),
                            objeto.getString("location"),
                            objeto.getString("unrestricted_quantity"),
                            objeto.getString("block_quantity"));

                    materials.add(material);

                } catch (JSONException e) {

                    Log.e(TAG, "Error en parsear: " + e.getMessage());

                    Toast.makeText(this.ct, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                    // Toast.makeText(this.ct, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {

            e.printStackTrace();

            Log.e(TAG, "Error: " + e.getMessage());

            // Toast.makeText(this.ct, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

        return materials;
    }

}
