package com.example.ezrra.todo_list_ezrra;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ezrra on 17/03/16.
 *
 * Creando nuestra lista personalizada o adaptador
 * Por que no se creo el view para el adaptador o cada renglon de la listView ???
 */


public class TareaArrayAdapter extends ArrayAdapter<Tarea> {

    public TareaArrayAdapter(Context context, List<Tarea> objects) {
        // Pasamos en constructor y los datos que va mostrar
        super(context ,0, objects);
    }

    /**
     * Devuelve una vista
     * Posicion, la vista y el padre PARAMETROS
     * */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Obteniendo una instancia del inflater
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Salvando la referencia del View de la fila
        View listItemView = convertView;

        // Comprobando si el view no existe
        if (null == convertView) {
            // si no existe, entonces inflarlo con two_line_list_item
            // Antes era: two_line_list_item de android.R. ....
            listItemView = inflater.inflate(R.layout.image_list_item, parent, false);
        }

        // Obteniendo instancia de los TextView

        TextView titulo = (TextView)listItemView.findViewById(R.id.text1);
        TextView subtitulo = (TextView)listItemView.findViewById(R.id.text2);
        ImageView categoria = (ImageView)listItemView.findViewById(R.id.category);

        // Obteniendo instancia de la tarea en la posicion actual
        Tarea item = getItem(position);

        titulo.setText(item.getNombre());
        subtitulo.setText(item.getHora());
        categoria.setImageResource(item.getCategoria());


        // Codigo Viejo
        // Divir la cadena en nombre y hora
        /* String cadenaBruta;
        String subCadenas [];
        String delimitar = ",";
        cadenaBruta = item.toString();
        subCadenas = cadenaBruta.split(delimitar, 2);
        titulo.setText(subCadenas[0]);
        subtitulo.setText(subCadenas[1]);*/
        // Codigo Viejo End

        // devolver al ListView la fila creada

        return listItemView;
    }



}
