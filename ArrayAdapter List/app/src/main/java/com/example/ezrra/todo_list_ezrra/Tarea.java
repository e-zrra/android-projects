package com.example.ezrra.todo_list_ezrra;

/**
 * Created by ezrra on 17/03/16.
 */
public class Tarea {
    private String nombre;
    private String hora;
    private int categoria;

    // Constructor
    public Tarea (String nombre, String hora, int categoria) {

        this.nombre = nombre;
        this.hora = hora;
        this.categoria =   categoria;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setHora (String hora) {
        this.hora = hora;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getCategoria() { return categoria; }

    public String getNombre () { return nombre; }

    public String getHora () { return hora; }

    @Override
    public String toString() { return nombre + "," + hora; }
}
