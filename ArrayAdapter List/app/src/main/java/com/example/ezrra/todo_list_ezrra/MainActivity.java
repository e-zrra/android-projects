package com.example.ezrra.todo_list_ezrra;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView;

public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener  {

    ListView lista;
    ArrayAdapter adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Instancia el list View
        lista = (ListView)findViewById(R.id.lista);

        // Inicializamos el adaptador con la fuente de datos

        adaptador = new TareaArrayAdapter(this, DataSource.TAREAS);

        //Relacionando la lista con el adaptador

        lista.setAdapter(adaptador);

        // Estableciendo la lista Ejemplo

        // lista.setOnItemClickListener(tuEscucha);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Se reingreso otro codigo
        int id = item.getItemId();
        if (id == R.id.action_clear) {
            // Limpiar todos los elementos
            adaptador.clear();
            return true;
        }
        return super.onOptionsItemSelected(item);


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /* if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item); */
    }


    /**
     * Se inserto el codigo implements AdapterView.OnItemClickListener
     * en esta clase MainActivity
     * y Elimino el error de NO ES SUPER CLASE en onItemClick
     * y se import AdapterView
     */

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Tarea tarea_actual = (Tarea)adaptador.getItem(position);
        String message = "Elegiste la tarea:n" + tarea_actual.getNombre() + " - " + tarea_actual.getHora();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
