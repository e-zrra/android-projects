package com.example.ezrra.spinner_ezrra;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener; // Necesario para el itemClick

// implements OnItemSelectedListener => Necesario para el itemClick
public class MainActivity extends AppCompatActivity implements OnItemSelectedListener {
    /*
    Estos atributos representan la posición y selección actual del Spinner
     */
    protected int position;
    protected String selection;

    // ArrayAdapter para conectar el Spinner a nuestros recursos string.xml
    protected ArrayAdapter<CharSequence> adapter;
    public static final int DEFAULT_POSITION = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtener instancia del GameSpinner
        Spinner spinner = (Spinner)findViewById(R.id.GameSpinner);



        // Asignar el origin de datos desde los recursos
        adapter = ArrayAdapter.createFromResource(this, R.array.Games, android.R.layout.simple_spinner_item);

        // Asignas el layout a inflar para cada elemento
        // Al momento de desplegar la lista
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Seteas el adaptador
        spinner.setAdapter(adapter);

        //Estableciendo la posición por defecto
        spinner.setSelection(DEFAULT_POSITION);

        /* Relacionando la escucha */
        spinner.setOnItemSelectedListener(this);




        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }); */
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        // Salvar la posicion y valor del item actual
        this.position = position;
        selection = parent.getItemAtPosition(position).toString();

        // Mostramos la opcion actual del Spinner
        Toast.makeText(this, "Seleccion actual " + selection, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        /*
        Nada por hacer
         */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
